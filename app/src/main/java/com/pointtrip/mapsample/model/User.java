package com.pointtrip.mapsample.model;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by krusher7 on 10/14/2016.
 */
@IgnoreExtraProperties
public class User {

    public String username;
    public String email;

    public User() {

    }

    public User (String username, String email) {
        this.username = username;
        this.email = email;
    }
}
