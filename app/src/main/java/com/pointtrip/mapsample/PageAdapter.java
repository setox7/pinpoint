package com.pointtrip.mapsample;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.pointtrip.mapsample.fragments.DirectionFragment;
import com.pointtrip.mapsample.fragments.MapFragment;
import com.pointtrip.mapsample.fragments.NewsFragment;
import com.pointtrip.mapsample.fragments.ProfileFragment;

/**
 * Created by krusher7 on 10/14/2016.
 */

public class PageAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;


    public PageAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                MapFragment map = new MapFragment();
                return map;
            case 1:
                NewsFragment news = new NewsFragment();
                return news;
            case 2:
                DirectionFragment direction = new DirectionFragment();
                return direction;
            case 3:
                ProfileFragment profile = new ProfileFragment();
                return profile;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

